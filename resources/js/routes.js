import Dashboard from "./components/Dashboard";
import calendrie from "./components/calendrie";
import Profile from "./components/Profile";
import Index from "./components/Users/Index";
import NotFound from "./components/NotFound";
import Invoice from "./components/Invoice";
import Personnel from "./components/Personnel";
import Reunion from "./components/Reunion";
import Conge from "./components/Conge";
import Paiement from "./components/Paiement";
import Emploi from "./components/Emploi";
export default {
    mode: "history",
    routes: [
        {
            path: "/admin/dashboard",
            component: Dashboard
        },
        {
            path: "/admin/profile",
            component: Profile
        },
        {
            path: "/admin/users",
            component: Index
        },
        {
            path: "/admin/conges",
            component: Conge
        },
        {
            path: "/admin/personnel",
            component: Personnel
        },
        {
            path: "/admin/reunions",
            component: Reunion
        },
        {
            path: "/admin/paiements",
            component: Paiement
        },
        {
          path: '/admin/emplois',
          component: Emploi
          },
          {
            path: '/admin/show-events',
            component: calendrie
            },

        {
            path: "*",
            component: NotFound
        }
    ]
};
