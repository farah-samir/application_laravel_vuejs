<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// middleware('auth:sanctum')->
Route::get('/users', 'Admin\API\UserController@index');
Route::post('/user/store', 'Admin\API\UserController@store');
Route::post('/user/update/{id}', 'Admin\API\UserController@update');

Route::get('/user-profile', 'Admin\API\UserController@profile');
Route::post('user-profile/update/{user}', 'Admin\API\UserController@updateUser');

Route::post('/user/{id}', 'Admin\API\UserController@destroy');
Route::get('findUser/{user}', 'Admin\API\UserController@findUser');


//personne
Route::get('/personnels', 'Admin\API\PersonnelController@index');
Route::post('/personnel/store', 'Admin\API\PersonnelController@store');
Route::post('/personnel/update/{id}', 'Admin\API\PersonnelController@update');
Route::post('/personnel/{id}', 'Admin\API\PersonnelController@destroy');
Route::get('findPersonnel/{user}', 'Admin\API\PersonnelController@findPersonnel');

//conge
Route::get('/conges', 'Admin\API\CongeController@index');
Route::post('/conge/store', 'Admin\API\CongeController@store');
Route::post('/conge/update/{id}', 'Admin\API\CongeController@update');
Route::post('/conge/{id}', 'Admin\API\CongeController@destroy');
Route::get('findConge/{conge}', 'Admin\API\CongeController@findConge');

//
//paeiement
Route::get('/paiements', 'Admin\API\PaiementController@index');
Route::post('/paiement/store', 'Admin\API\PaiementController@store');
Route::post('/paiement/update/{id}', 'Admin\API\PaiementController@update');
Route::post('/paiement/{id}', 'Admin\API\PaiementController@destroy');
Route::get('findPaiement/{paiement}', 'Admin\API\PaiementController@findPaiement');

//reunion
Route::get('/reunions', 'Admin\API\ReunionController@index');
Route::post('/reunion/store', 'Admin\API\ReunionController@store');
Route::post('/reunion/update/{id}', 'Admin\API\ReunionController@update');
Route::post('/reunion/{id}', 'Admin\API\ReunionController@destroy');
Route::get('findReunion/{reunion}', 'Admin\API\ReunionController@findReunion');

//
Route::get('/emplois', 'Admin\API\EmploiController@index');
Route::post('/emploi/store', 'Admin\API\EmploiController@store');
Route::post('/emploi/update/{id}', 'Admin\API\EmploiController@update');
Route::post('/emploi/{id}', 'Admin\API\EmploiController@destroy');
Route::get('findEmploi/{emploi}', 'Admin\API\EmploiController@findEmploi');
  // calendrie
  Route::get('/show-events', [CalendarController::class, 'index']);

Route::get('/show-events', [CalendarController::class, 'calendarEvents']);