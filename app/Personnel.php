<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    protected $fillable = [
        'personne', 'telephone', 'bio','type',
    ];

    public function paiements(){
        return $this->hasMany('app\paiement');
      }

      public function reunions(){
        return $this->hasMany('app\reunion');
      }


}
