<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class EventList extends Model
{
    use HasFactory;
    protected $fillable = [
        'event_name',
        'event_start',
        'event_end'
    ];
}



