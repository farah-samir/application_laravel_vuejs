<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conge extends Model
{
    protected $fillable = [
        'nom', 'prenom', 'cin','date_entree','date_sortie'
    ];
}
