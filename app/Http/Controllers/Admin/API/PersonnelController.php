<?php

namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Personnel;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class PersonnelController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');

    }
    public function index()
    {
        $Personnels = Personnel::paginate(10);
        return response()->json($Personnels);
    }
    public function store(Request $request)
    {
            $request->validate([
                'personne' => 'bail|required|max:20',
                'telephone' => ['bail','required', 'string', 'max:255',
                Rule::unique('personnels')],
                'bio' => 'nullable|max:50',
                'type' => 'required',
               

            ]);
            $personnel = Personnel::create([
                'personne' => $request->personne,
                'telephone' => $request->telephone,
                'bio' => $request->bio,
                'type' => $request->type,
              
            ]);

        return response()->json($personnel);

    }
    public function update(Request $request ,$id)
    {
        // if(Gate::allows('isAdmin')){
        $request->validate([
            'personne' => 'bail|required|max:30',
            'telephone' => ['bail','required', 'string',  'max:255'],
            'bio' => 'nullable|max:50',
            'type' => 'required',
            
        ]);
        $personnel = Personnel::findOrFail($id);
        $personnel->update([
            'personne' => $request->personne,
            'telehone' => $request->telehone,
            'type' => $request->type,
          
            'bio' => $request->bio,
        ]);
        return response()->json($personnel);
    }
    public function profile()
    {
        // return ['name'=> 'Hadayat Niazi'];
    }
    // public function updatePersnnel(Request $request, $id)
    // {

    //     $request->validate([
    //         'personne' => 'bail|required|max:20',
    //         'bio' => 'nullable|max:80',
    //         'type' => 'required',
    //         'telephone' => ['bail','required', 'string', 'max:255']
           
    //     ]);

    //     $personnel = Personnel::findOrFail($id);
    //     $personnel->update([
    //         'personne' => $request->personne,
    //         'telephone' => $request->telephone,
    //         'type' => $request->type,
           
    //         'bio' => $request->bio,
    //     ]);
    //     return response()->json($personnel);
    // }
    public function destroy($id)
    {
        $personnel = Personnel::findOrFail($id)->delete();
        return response()->json($personnel);
    }
    public function findPersonnel($personnel)
    {
            $personnel = Personnel::where('personne', 'LIKE', '%'.$personnel. '%')
            ->orwhere('telephone', 'LIKE', '%'.$personnel. '%')
            ->orwhere('type', 'LIKE', '%'.$personnel. '%')
            ->orwhere('bio', 'LIKE', '%'.$personnel. '%')
            ->paginate(10);

        return response()->json($personnel);
    }
}
