<?php
namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Emploi;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Image;

class EmploiController extends Controller
{
    public function index()
    {
        $emplois = Emploi::paginate(10);
        return response()->json($emplois);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
       
        $exploaded = explode(',', $request->image);
        $decoded = base64_decode($exploaded[1]);
        if(str_contains($exploaded[0],'jpeg'))
        $extension = 'jpg';
        
         else 
         $extension = 'pdf';
        $fileName = str_random().'.'.$extension;
        $path = public_path().'/'.$fileName;
        file_put_contents($path,$decoded);
        $emploi = Emploi::create($request->except('image') + [
            
           
            'image' => $fileName
            
            ]);
      
    }
    public function destroy($id)
    {
        $emploi = Emploi::findOrFail($id)->delete();
        return response()->json($emploi);
    }
    public function findEmploi($emploi)
    {
            $emploi = Emploi::where('name', 'LIKE', '%'.$emploi. '%')
            ->orwhere('poste', 'LIKE', '%'.$emploi. '%')
            ->orwhere('ville', 'LIKE', '%'.$emploi. '%')
            ->orwhere('salaire', 'LIKE', '%'.$emploi. '%')
            ->orwhere('remarque', 'LIKE', '%'.$emploi. '%')
            ->paginate(10);

        return response()->json($emploi);
    }
    public function update(Request $request ,$id, Emploi $emploi)
    {
        // if(Gate::allows('isAdmin')){
    

             $emploi = Emploi::findOrFail($id);


                    if($request->image!=$emploi->image){
                        $strpos = strpos($request->image,';');
                        $sub = substr($request->image,0,$strpos);
                     

                                     $exploaded = explode(',', $request->image);
                                        $decoded = base64_decode($exploaded[1]);
                                        if(str_contains($exploaded[0],'jpeg'))
                                        $extension = 'jpg';
                                        else 
                                        $extension = 'pdf';
                                        $fileName = str_random().'.'.$extension;
                                        $path = public_path().'/'.$fileName;
                                        file_put_contents($path,$decoded);  
                                        $img = $request->image;
                    
                    }
                    else{
                        $fileName = $emploi->image;
                    }
            
                    $emploi->poste = $request->poste;
                    $emploi->ville = $request->ville;
                    $emploi->salaire = $request->salaire;
                    $emploi->remarque =  $request->remarque;
                    $emploi->name =  $request->name;
                    $emploi->image = $fileName;
                    $emploi->save();



            return response()->json([
                'error' => 0,
                'message' => 'emploi has been updated',
                'data' => $emploi,
            ]);

      
                
                }

}
