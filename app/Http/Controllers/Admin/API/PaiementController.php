<?php

namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use App\Paiement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

use App\Personnel;

class PaiementController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');

    }
    public function index()

    {
       // $paiements = Paiement::latest()->with('personnel')->pagination(10);
        //return response()->json($paiements);


        $paiements = Paiement::with('personnel')->paginate(10);
        return response()->json($paiements);

    
    // $paiements = Paiement::orderBy('created_at', 'desc')->with('personnel')->get();
      //  echo json_encode($paiements);
        


    }
    public function store(Request $request)
    {
            $request->validate([
               
            

            ]);
            $paiement = Paiement::create([
               
                'personnel_id' => $request->personnel_id,
                'cin' => $request->cin,
                'prime' => $request->prime,
                'total' => $request->total,
                'paiemment_humaine' => $request->paiemment_humaine,
                'date_paie' => $request->date_paie,
                'mois' => $request->mois,
                'enabled' => $request->enabled
                
            ]);

        return response()->json($paiement);

    }
    public function update(Request $request ,$id)
    {
        // if(Gate::allows('isAdmin')){
    
        $paiement = Paiement::findOrFail($id);
        $paiement->update([
            'cin' => $request->cin,
            'personnel_id' => $request->personnel_id,
            'prime' => $request->prime,
            'total' => $request->total,
            'paiemment_humaine' => $request->paiemment_humaine,
            'date_paie' => $request->date_paie,
            'mois' => $request->mois,
            'enabled' => $request->enabled
        ]);
        return response()->json($paiement);
    }
    public function profile()
    {
        // return ['name'=> 'Hadayat Niazi'];
    }
    public function updatePaiement(Request $request, $id)
    {

        $request->validate([
            
            'prime' => 'bail','nullable',  'max:255',
            'paiemment_humaine' => 'nullable|max:50',
            'total' => 'nullable|max:50',
            'date_paie' => 'required',
            'mois' => 'nullable',
            'cin' => 'required|max:20'
        ]);

        $paiement = Paiement::findOrFail($id);
        $paiement->update([
            'personnel_id' => $request->personnel_id,
            'cin' => $request->cin,
            'prime' => $request->prime,
            'paiemment_humaine' => $request->paiemment_humaine,
            'total' => $request->total,
            'date_paie' => $request->date_paie,
            'mois' => $request->mois,
        ]);
        return response()->json($paiement);
    }
    public function destroy($id)
    {
        $paiement = Paiement::findOrFail($id)->delete();
        return response()->json($paiement);
    }
    public function findPaiement($paiement)
    {
            $paiement = Paiement::where('personnel_id', 'LIKE', '%'.$paiement. '%')
            ->orwhere('prime', 'LIKE', '%'.$paiement. '%')
            ->orwhere('paiemment_humaine', 'LIKE', '%'.$paiement. '%')
            ->orwhere('cin', 'LIKE', '%'.$paiement. '%')
            ->paginate(10);

        return response()->json($paiement);
    }
}
