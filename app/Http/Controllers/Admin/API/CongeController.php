<?php

namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use App\Conge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CongeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');

    }
    public function index()
    {
        $conges = Conge::paginate(10);
        return response()->json($conges);
    }
    public function store(Request $request)
    {
            $request->validate([
                'nom' => 'bail|required|max:20',
                'prenom' => ['bail','required', 'string', 'max:255'],
                'date_entree' => 'required',
                'date_sortie' => 'required',
                'cin' => 'required'

              


            ]);
            $conge = Conge::create([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'date_entree' => $request->date_entree,
                'date_sortie' => $request->date_sortie,
                'cin' => $request->cin,
            ]);

        return response()->json($conge);

    }
    public function update(Request $request ,$id)
    {
        // if(Gate::allows('isAdmin')){
        $request->validate([
            'nom' => 'bail|required|max:20',
                'prenom' => ['bail','required', 'string', 'max:255'],
                'date_entree' => 'required',
                'date_sortie' => 'required',
                'cin' => 'required'
        ]);
        $conge = Conge::findOrFail($id);
        $conge->update([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'date_entree' => $request->date_entree,
            'date_sortie' => $request->date_sortie,
            'cin' => $request->cin,
        ]);
        return response()->json($conge);
    }
    public function profile()
    {
        // return ['name'=> 'Hadayat Niazi'];
    }
    public function updateConge(Request $request, $id)
    {

        $request->validate([
            'nom' => 'bail|required|max:20',
            'prenom' => ['bail','required', 'string', 'max:255'],
            'date_entree' => 'required',
            'date_sortie' => 'required',
            'cin' => 'required|unique'
        ]);

        $conge = Conge::findOrFail($id);
        $conge->update([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'date_entree' => $request->date_entree,
            'date_sortie' => $request->date_sortie,
            'cin' => $request->cin,
        ]);
        return response()->json($user);
    }
    public function destroy($id)
    {
        $conge = Conge::findOrFail($id)->delete();
        return response()->json($conge);
    }
    public function findUser($conge)
    {
            $conge = Conge::where('nom', 'LIKE', '%'.$conge. '%')
            ->orwhere('prenom', 'LIKE', '%'.$conge. '%')
            ->orwhere('cin', 'LIKE', '%'.$conge. '%')
           
            ->paginate(10);

        return response()->json($conge);
    }
}
