<?php

namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Reunion;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ReunionController extends Controller
{
    


    public function __construct()
    {
        // $this->middleware('auth:api');

    }
    public function index()

    {
       // $paiements = Paiement::latest()->with('personnel')->pagination(10);
        //return response()->json($paiements);


        $reunions = Reunion::with('personnel')->paginate(10);
        return response()->json($reunions);

    
    // $paiements = Paiement::orderBy('created_at', 'desc')->with('personnel')->get();
      //  echo json_encode($paiements);
        


    }
    public function store(Request $request)
    {
            $request->validate([
               
            

            ]);




            $reunion = Reunion::create([
               
                'personnel_id' => $request->personnel_id,
                'objet' => $request->objet,
                'date_reunion' => $request->date_reunion,
                'heure' => $request->heure,
                'mois' => $request->mois,
                'sujet' => $request->sujet,
                
            ]);

        return response()->json($reunion);

    }
    public function update(Request $request ,$id)
    {
        // if(Gate::allows('isAdmin')){
    
        $reunion = Reunion::findOrFail($id);
        $reunion->update([
            'personnel_id' => $request->personnel_id,
                'objet' => $request->objet,
                'date_reunion' => $request->date_reunion,
                'heure' => $request->heure,
                'mois' => $request->mois,
                'sujet' => $request->sujet,
        ]);
        return response()->json($reunion);
    }
    public function profile()
    {
        // return ['name'=> 'Hadayat Niazi'];
    }
    public function updateReunion(Request $request, $id)
    {

        $request->validate([
            
            'sujet' => 'bail','required',  'max:255',
            'objet' => 'nullable|max:50',
            'date_reunion' => 'required',
            'mois' => 'nullable',
            'sujet' => 'required|max:20'
        ]);

        $reunion = Reunion::findOrFail($id);
        $reunion->update([
            'personnel_id' => $request->personnel_id,
            'objet' => $request->objet,
            'date_reunion' => $request->date_reunion,
            'heure' => $request->heure,
            'mois' => $request->mois,
            'sujet' => $request->sujet,
        ]);
        return response()->json($reunion);
    }
    public function destroy($id)
    {
        $reunion = Reunion::findOrFail($id)->delete();
        return response()->json($reunion);
    }
    public function findPaiement($reunion)
    {
            $reunion = Reunion::where('personnel_id', 'LIKE', '%'.$reunion. '%')
            ->orwhere('sujet', 'LIKE', '%'.$reunion. '%')
            ->orwhere('mois', 'LIKE', '%'.$reunion. '%')
            ->orwhere('mois', 'LIKE', '%'.$reunion. '%')
            ->paginate(10);

        return response()->json($reunion);
    }





}
