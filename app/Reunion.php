<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    
    protected $fillable = [
        'personnel_id', 'objet','sujet','date_reunion','mois','heure'
    ];
    
    public function Personnel()
    {
        return $this->belongsTo(Personnel::class);
    }


}
