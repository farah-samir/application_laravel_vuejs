<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emploi extends Model
{
    protected $fillable = [
        'name', 'poste','ville','salaire','remarque','image'
    ];
}
